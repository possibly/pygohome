import scrapy
from ..mailr import mail
from ..items import CraigslistItem

# Handles gathering and parsing the correct data from the 
# allowed domains

#&minAsk=1&maxAsk=2&bedrooms=1&bathrooms=1

class CraigslistSpider(scrapy.Spider):
    name = "craigslist"
    allowed_domains = ["sfbay.craigslist.org"]
    start_urls = [
        "http://sfbay.en.craigslist.org/sby/apa/"
    ]

    def parse(self, response):   
        #Search criteria
        minAsk = None
        maxAsk = None
        bedrooms = None
        bathrooms = None
        cliList = [] 	
        hack = 1 #the bedrooms and sqft is simply a string so this is a hack to 
                 #select it using an xpath query generated in Chrome
        for item in response.css('.row .txt'):
            cli = CraigslistItem()

            #TODO: input should change request, not parsing.
            potentialPrice = item.css('.price').xpath('text()').extract()[0][1:]
            if(CraigslistSpider.minAsk is None and CraigslistSpider.maxAsk is None):
                cli['price'] = potentialPrice

            elif(CraigslistSpider.minAsk is not None and CraigslistSpider.maxAsk is not None):
                if(int(potentialPrice) > CraigslistSpider.minAsk and int(potentialPrice) < CraigslistSpider.maxAsk):
                    cli['price'] = potentialPrice

            elif(CraigslistSpider.minAsk is not None):
                if(int(potentialPrice) > CraigslistSpider.minAsk):
                    cli['price'] = potentialPrice

            elif(CraigslistSpider.maxAsk is not None):
                if(int(potentialPrice) < CraigslistSpider.maxAsk):
                    cli['price'] = potentialPrice
            


            cli['title'] = item.css('.pl a').xpath('text()').extract()[0]
            cli['linkToPosting'] = 'www.craigslist.org'+item.css('.pl a').xpath('@href').extract()[0]
            # cli['price'] = item.css('.price').xpath('text()').extract()[0]
            cli['location'] = unicode(item.css('small').extract())[12:-11]

            bedroomsAndSqFt = item.xpath('//*[@id="toc_rows"]/div[2]/p['+str(hack)+']/span/span[3]/text()[2]').extract()[0][3:-7]
            hack = hack+1

            # cli['squareFootage'] = bedroomsAndSqFt
            cli['bedrooms'] = bedroomsAndSqFt[:3]
            cli['squareFootage'] = bedroomsAndSqFt[5:]

            cliList.append(cli)

        # for p in cliList:
        #     print p
        #     print '\n'
        # return

        # template = makeTemplate(cliList)
        # return mail(cliList,template)

        return cliList