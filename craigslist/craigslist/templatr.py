from jinja2 import Environment, PackageLoader

# Handles making templates
# NOTE: Templates like unicode.

def makeTemplate(self):
	env = Environment(loader=PackageLoader('craigslist.craigslist','templates'))
  	template = env.get_template('msgr.html')
  	return template.render(cliList = self)