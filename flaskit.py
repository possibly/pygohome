from flask import Flask, render_template,request
from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy.settings import Settings
from scrapy import log, signals
from scrapy.xlib.pydispatch import dispatcher
from craigslist.craigslist.spiders.craigslist_spider import CraigslistSpider
from craigslist.craigslist.templatr import makeTemplate

app = Flask(__name__)

cliList = []
def add_item(item):
	cliList.append(item)

def stop_reactor():
    reactor.stop()

@app.route('/',methods=['GET'])
def hello(cli=""):
    return render_template('index.html',cli=cli)

@app.route('/post',methods=['POST'])
def test():
	price = request.form['price']

	# dispatcher.connect(add_item, signal=signals.item_scraped)
	dispatcher.connect(stop_reactor, signal=signals.spider_closed)

	#TODO: Thread this
	spidr = CraigslistSpider()
	spidr.maxAsk = price
	crawler = Crawler(Settings())
	crawler.configure()
	crawler.crawl(spidr)
	crawler.start()
	log.start()
	reactor.run() # blocking, continues when done

	return render_template('index.html',cli=spidr.maxAsk)

if __name__ == '__main__':
	app.run(debug=True)
